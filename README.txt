 .. contents::

Introduction
============

Instalación
===========

Papel Digital reqiere instalar los siguientes módulos de sistema:

- GraphicsMagick para manipular imágenes.

    sudo apt-get install graphicsmagick

- Ghostscript para procesar archivos PS y PDF.

    sudo apt-get install ghostscript

- Poppler para manipular PDF

    sudo apt-get install libpoppler-dev libpoppler5 poppler-utils

- Tesseract para hacer OCR (Opcional)

    sudo apt-get install tesseract-ocr tesseract-ocr-spa tesseract-ocr-eng tesseract-ocr-dev

- Open Office, para ver DOC, PPT, etc.

    sudo apt-get install openoffice.org

- Doc Split, para separar documentos en páginas.

    sudo gem install docsplit



