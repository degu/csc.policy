from setuptools import setup, find_packages
import os

version = '0.1.1'

long_description = (
    open('README.txt').read()
    + '\n' +
    'Contributors\n'
    '============\n'
    + '\n' +
    open('CONTRIBUTORS.txt').read()
    + '\n' +
    open('CHANGES.txt').read()
    + '\n')

setup(name='csc.policy',
      version=version,
      description="Paquete principal para el sitio del Centro de Sangre Concepcion",
      long_description=long_description,
      # Get more strings from
      # http://pypi.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
        "Environment :: Web Environment",
        "Framework :: Plone",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.6",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
      keywords='',
      author='',
      author_email='',
      url='http://svn.plone.org/svn/collective/',
      license='gpl',
      packages=find_packages('src'),
      package_dir={'': 'src'},
      namespace_packages=['csc', ],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          # JQueryUI
          'collective.js.jqueryui',
          # Lingua Plone
          #'Products.LinguaPlone',
          # Red Turtle Video
          'redturtle.video',
          'collective.rtvideo.youtube',
          # Plone True Gallery
          'collective.plonetruegallery',
          'collective.ptg.highslide',
          'collective.ptg.fancybox',
          'collective.ptg.galleriffic',
          'collective.ptg.s3slider',
          'collective.ptg.pikachoose',
          'collective.ptg.nivoslider',
          'collective.ptg.nivogallery',
          'collective.ptg.contentflow',
          'collective.ptg.supersized',
          'collective.ptg.thumbnailzoom',
          'collective.ptg.contactsheet',
          # Like button
          'sc.social.like',
          # Document viewer
          'collective.documentviewer',
          # Para crear portlets con objetos embebidos
          'collective.portlet.embed',
          'csc.theme',
          'csc.content',
      ],
      extras_require={'test': ['plone.app.testing']},
      entry_points="""
      # -*- Entry points: -*-
      [z3c.autoinclude.plugin]
      target = plone
      """,
      setup_requires=["PasteScript"],
      paster_plugins=["templer.localcommands"],
      )
