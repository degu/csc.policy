from plone.app.testing import PloneWithPackageLayer
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting

import csc.policy


CSC_POLICY = PloneWithPackageLayer(
    zcml_package=csc.policy,
    zcml_filename='testing.zcml',
    gs_profile_id='csc.policy:testing',
    name="CSC_POLICY")

CSC_POLICY_INTEGRATION = IntegrationTesting(
    bases=(CSC_POLICY, ),
    name="CSC_POLICY_INTEGRATION")

CSC_POLICY_FUNCTIONAL = FunctionalTesting(
    bases=(CSC_POLICY, ),
    name="CSC_POLICY_FUNCTIONAL")
